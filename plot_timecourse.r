#!/usr/bin/env Rscript

source("plot_util.R")
#library(directlabels)
args = commandArgs(trailingOnly=TRUE)
file = parse_args(args,"singlestep_resol_time_course.txt")
mydata = read.csv(file ,sep="\t",skip = 1)
range = max(mydata$ac)
str_length = max(mydata$me)
len =  nrow(mydata)
mdata = melt (data = mydata, id.vars = "time" )
maxtime = max(mdata$time)
maxtime
generations = 6
interval = maxtime/generations 
interval
	
##  plots 
margin <- ggplot(mdata, aes(x=time, y = value)) +  
	geom_line(aes(color = variable),size = 1.5) +
  	#geom_vline(xintercept=seq(interval,maxtime -interval, interval  ), linetype="dotted") +
	scale_fill_discrete(name = "Dose") +
	labs(y = "cells fraction", x = "time [au]",fill = "test" )  +
	theme(legend.justification = c(1, 1), legend.position = c(1, 1)) 
#direct.label(margin, method="last.points")

outfile = mutate_filename(file,"")
pdf(outfile)
plot(margin)
dev.off()
set_exifdata (file, outfile)

system( paste("okular ",outfile))
