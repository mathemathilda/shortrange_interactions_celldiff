#!/usr/bin/env Rscript

source("plot_util.R")
#library(directlabels)
args = commandArgs(trailingOnly=TRUE)

if (length(args)==1) {
	par1 = 1
	par2 = 2

} else if (length(args)== 3  ){
	par1 = as.numeric(args[2])
	par2 = as.numeric(args[3])
}
file = parse_args(args,"singlestep_resol_time_course.txt")
mydata = read.csv(file ,sep="\t",skip = 1)

labels = colnames(mydata) 
variate   <- labels[par1+3]
variate
covariate <- labels[par2+3]
covariate
labels[par1+3] = "c_variate"
labels[par2+3] = "c_covariate"
colnames(mydata) = labels

	
##  plots 
margin <- ggplot(mydata, aes(x=c_variate, y = c_covariate)) +  
	#geom_line(aes(color = "blue"),size = 1.5) +
	geom_line() +
	#labs(y = "counts", x = "time in arb.unit") +
	labs(x = variate, y = covariate)+
	theme(legend.justification = c(1, 1), legend.position = c(1, 1)) 
#direct.label(margin, method="last.points")

outfile = mutate_filename(file,"")
pdf(outfile)
plot(margin)
dev.off()
set_exifdata (file, outfile)

system( paste("evince ",outfile))
