import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
# colorspace: https://python-colorspace.readthedocs.io/en/stable/installation.html
from colorspace.colorlib import HCL
import pandas
import csv
import sys
import subprocess

if len(sys.argv) < 2 :
    filename= "parameter_variation_opt_run32_tAc5_random_me_adder.txt"
else:
    filename = sys.argv[1]

data = pandas.read_csv(filename, sep = "\t",header=1 )


diff = data["lagging_me"] - data["leading_me"]
diff.name = 'diff'

lead_me = data["leading_me"]
lead_me_sd_p = data["leading_me"] + data["leading_me_sd"]
lead_me_sd_n = data["leading_me"] - data["leading_me_sd"]

lagging_me = data["lagging_me"]
lagging_me_sd_p = data["lagging_me"] + data["lagging_me_sd"]
lagging_me_sd_n = data["lagging_me"] - data["lagging_me_sd"]

lead_ac = data["leading_ac"]
lead_ac_sd_p = data["leading_ac"] + data["leading_ac_sd"]
lead_ac_sd_n = data["leading_ac"] - data["leading_ac_sd"]

lagging_ac = data["lagging_ac"]
lagging_ac_sd_p = data["lagging_ac"] + data["lagging_ac_sd"]
lagging_ac_sd_n = data["lagging_ac"] - data["lagging_ac_sd"]

ordinate = data.iloc[:,0]
variate= ordinate.name
ordinate.name='variate'
#plotdata= pandas.concat([ordinate, lead_me, lagging_me, diff ], axis=1, sort=False)

# tetradic colours:
k = 140 # start hue
j = 50  # distance close colours
c = 70  # chroma
l = 60  # lightness
cols = HCL(H = [ k, k+j, k+180, k+180+j, 0 ],
           C = [ c,  c ,  c   ,  c   , 0 ],
           L = [ l,  l ,  l   ,  l   , 30 ])

# methylation
plt.plot(ordinate,lead_me,cols(1)[0])
plt.fill_between(ordinate,lead_me_sd_n, lead_me_sd_p , color = cols(1)[0] , alpha=0.3)
plt.plot(ordinate,lagging_me,cols(1)[1])
plt.fill_between(ordinate,lagging_me_sd_n , lagging_me_sd_p , color = cols(1)[1] , alpha=0.3 )
# acetylation
plt.plot(ordinate,lagging_ac,cols(1)[2])
plt.fill_between(ordinate,lagging_ac_sd_n, lagging_ac_sd_p,color=cols(1)[2],
                 alpha=0.3 )
plt.plot(ordinate,lead_ac,cols(1)[3])
plt.fill_between(ordinate,lead_ac_sd_n, lead_ac_sd_p , color = cols(1)[3] , alpha=0.3 )
plt.plot(ordinate,diff,cols(1)[4])

plt.xlabel(variate)
plt.ylabel("fraction of final state categorization")


lead_me_legend = mpatches.Patch(color=cols(1)[0], label='lead strand me')
lag_me_legend = mpatches.Patch(color=cols(1)[1], label='lag strand me')
lag_ac_legend =mpatches.Patch(color=cols(1)[2],label='lag strand ac')
lead_ac_legend =mpatches.Patch(color=cols(1)[3],label='lead strand ac')
diff_legend =mpatches.Patch(color=cols(1)[4],label='difference ac')
plt.legend(handles=[lag_me_legend,lead_me_legend,lag_ac_legend,lead_ac_legend,diff_legend])
#plt.legend([lead_me,lead_ac,lagging_me,lagging_ac,diff],
#           ["leading strand me","leading strand ac",
#           "lagging strand me","lagging_strand_ac","difference ac"])

#myplot = plotdata.plot(x = 'variate')
#myplot.set_xlabel(variate)

outfile= filename.split(".")[0] + "_plot.pdf"
plt.savefig(outfile)
bashCommand = "okular " + outfile
process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
output, error = process.communicate()


#plt.figure();
#data.plot();


