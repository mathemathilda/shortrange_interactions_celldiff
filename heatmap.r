df = read.table("results.txt")

library(reshape2); library(ggplot2)
rnames = c(1:nrow(df))
library(stringr)
rnames = str_pad(rnames, 3, pad = "0")
rownames(df) <- rnames
colors <- c( "green","yellow", "red")
categories =  c("un","ac","me")
# names(colors) <- categories

## remove background and axis from plot
theme_change <- theme(
 plot.background = element_blank(),
 panel.grid.minor = element_blank(),
 panel.grid.major = element_blank(),
 panel.background = element_blank(),
 panel.border = element_blank(),
 axis.line = element_blank(),
 axis.ticks = element_blank(),
 axis.text.x = element_blank(),
 axis.text.y = element_blank()
)




ggplot(melt(cbind(generation=rownames(df), df)),
    aes(x = variable, y = generation,  fill = factor(value) )) +
    geom_tile() +
    xlab("position") +
    coord_fixed(ratio = 1) +
    # scale_fill_manual(values=colors, name = "modification",  )+
    scale_fill_manual(
		      name = "nucleosome state",
  values = colors,
  breaks = c(0,1,2),
  labels = c("un", "ac", "me")
) + theme_change
