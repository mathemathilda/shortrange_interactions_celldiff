#!/usr/bin/env Rscript

source("plot_util.R")

args = commandArgs(trailingOnly=TRUE)
file = parse_args(args,"")
mydata = read.csv(file ,sep="\t",skip = 1)
mydata = mydata[c(1,2,4)]
range = max(mydata$ac)
str_length = max(mydata$me)
len =  length(mydata[,1])
exploded <- data.frame(me = numeric(sum(mydata$count)), ac=numeric(sum(mydata$count) ) )
runner = 1
for (i in 1:len) {
	if (  mydata$count[i] != 0){
		for ( c in 1:mydata$count[i]) {
			exploded$me[runner] <- str_length* asin(mydata$me[i]/ sqrt(mydata$me[i]^2 + mydata$ac[i]^2))
			runner <- runner +1
		}
	}
}
	
##  plots 
margin <- ggplot(data=exploded, aes(x=me)) +  
	geom_histogram(binwidth = 1)   +
	scale_x_continuous(labels = function(x) paste0(x, "%"))
	#scale_x_continuous(labels = scales::percent_format(accuracy = 1))
	#scale_x_continuous(labels = scales::percent) 
#grid.arrange(margin, density, nrow=2)
outfile = mutate_filename(file,"1D")
pdf(outfile)
plot(margin)
dev.off()

set_exifdata (file, outfile)
system( paste("okular ",outfile))
