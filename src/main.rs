#![allow(unused)]
extern crate ndarray;
extern crate rand;
extern crate regex;
extern crate termion;
//use ndarray::{Array1, ArrayView1, arr1};
use ndarray::prelude::*;
use rand::distributions::Alphanumeric;
use rand::seq::SliceRandom;
use rand::{rngs::StdRng, thread_rng, Rng, SeedableRng};
//use rand::{thread_rng, Rng, SeedableRng};
use rayon::prelude::*;
use regex::Regex;
use rustats::hypothesis_testings;
//use rustats::hypothesis_testings::MannWhitneyU;
use serde::{Deserialize, Serialize};
use statistical;
use std::cmp;
use std::convert::TryFrom;
use std::env;
use std::error::Error;
use std::f32;
use std::fs;
use std::fs::File;
use std::fs::OpenOptions;
use std::io::prelude::*;
use std::mem;
use std::ops::Neg;
use std::process;
use std::{thread, time};
use termion::{color, style};
#[derive(Default, Debug, Clone, Serialize, Deserialize)]
struct Parameters {
    start_states: Vec<Position>,
    gene_activation_threshold: usize,
    origins_of_replication: Vec<usize>,
    regions_of_interest: Vec<Vec<usize>>,
    cooperative_add_me_ac: [f64; 2],
    cooperative_del_me_ac: [f64; 2],
    random_addition_me_ac: [f64; 2],
    random_deletion_me_ac: [f64; 2],
    parameter_variation: String,
    parameter_variation2dim: String,
    intervals: usize,
    sampling: String,
    print_steps: bool,
    folder: String,
    name: String,
    depletion_bias: f64,
    runs: usize,
    generations: usize,
    stringsize: usize,
    enzymereach: usize,
    time: f64,
    interval_end: f64,
    interval_end_2dim: f64,
}
#[derive(Clone, Copy, Debug, Serialize, Deserialize)]
struct Position {
    me: usize,
    ac: usize,
}
struct Simvars {
    boarder_lookup: Vec<Vec<usize>>,
    simulation_counter: usize,
    interval_counter: usize,
    lead_lag_mask: Vec<usize>,
    start_time: time::Instant,
    single_run_time: time::Instant,
    histstring: Vec<usize>,
    end_position_count_vec: Vec<Vec<Vec<usize>>>,
    activation_time: Vec<f64>, // of single run
    delta: Vec<f64>,
    rng: rand::rngs::StdRng,
    iterations: usize,
}

const ME: usize = 0; // needs to be smaller than AC since used in for loop
const AC: usize = 1;
const UN: usize = 2;
const CELL_A: usize = 0;
const CELL_B: usize = 1;
const GENE_A: usize = 0;
const GENE_B: usize = 1;

fn main() -> Result<(), Box<dyn Error>> {
    let args: Vec<String> = std::env::args().collect();
    let mut run_number: u64 = 0;
    match args.len() {
        1 => println!("please supply run number"),
        2 => {
            run_number = match args[1].parse() {
                Ok(n) => n,
                Err(_) => {
                    eprintln!(" run number argument must be integer number");
                    std::process::exit(1)
                }
            };
        }
        _ => {
            eprintln!(" run number argument must be integer number");
            std::process::exit(0x0001)
        }
    }

    let custom_start_states = vec![
        Position { me: 51, ac: 0 },
        Position { me: 52, ac: 0 },
        Position { me: 53, ac: 0 },
        Position { me: 54, ac: 0 },
        Position { me: 55, ac: 0 },
        Position { me: 56, ac: 0 },
        Position { me: 57, ac: 0 },
        Position { me: 58, ac: 0 },
        Position { me: 59, ac: 0 },
        Position { me: 60, ac: 0 },
    ];

    let mut cfg = Parameters {
        start_states: custom_start_states,
        gene_activation_threshold: 60, // in percent
        origins_of_replication: vec![30],
        regions_of_interest: vec![vec![10, 20], vec![40, 50]],
        cooperative_add_me_ac: [0.0024, 0.016],
        cooperative_del_me_ac: [0.05, 0.007],
        random_addition_me_ac: [0.5, 2.0],
        random_deletion_me_ac: [0.01, 0.01],
        parameter_variation: "random_ac_deleter".to_string(), // "none","descent","random" or  parameter to be varied
        parameter_variation2dim: "cooperative_ac_adder".to_string(), // "none","descent","random" or  parameter to be varied
        intervals: 30,                                                 // sampling interval
        sampling: "single run".to_string(), // " single_run", "proliferation"
        print_steps: false,                 // for time series
        folder: "".to_string(),
        name: "opt".to_string(),
        depletion_bias: 0.6,
        runs: 100, // needs to be greater than 1 for standard deviations
        generations: 1,
        stringsize: 60,
        enzymereach: 10,
        time: 500.0,
        interval_end: 1000.0, // starting stepsize for "descent"; parameter variation between interval_start, and somewhere, with current value being in the center
        interval_end_2dim: 1000.0,
    };
    let ac_start_states = vec![Position {
        me: 0,
        ac: cfg.stringsize,
    }];
    let all_start_states = generate_all_states(&cfg);

    let mut sv = Simvars {
        boarder_lookup: get_boarder_lookup(&cfg), // precompute to spare bifurcations
        simulation_counter: 0,
        interval_counter: 0,
        lead_lag_mask: oris2lead_lag_mask(&cfg.origins_of_replication, &[CELL_B, CELL_A], &cfg),
        start_time: time::Instant::now(),
        single_run_time: time::Instant::now(),
        histstring: vec![UN; cfg.stringsize],
        end_position_count_vec: vec![vec![vec![0; 2]; cfg.stringsize + 1]; cfg.stringsize + 1],
        activation_time: vec![0.0; 2], // for two regions of interest aka genes
        delta: vec![0.0; 8],
        rng: SeedableRng::seed_from_u64(run_number),
        iterations: 0,
    };

    let randstr: String = thread_rng().sample_iter(&Alphanumeric).take(4).collect();
    cfg.name
        .push_str(&format!("_run{}_{}", run_number, randstr));
    let re = Regex::new(r"(?P<par>[[:alpha:]_]+:)").unwrap();
    let parms = format!("{:?}", cfg);
    let linewise_cfg = re.replace_all(&parms, "\n$par");
    println!("{}", linewise_cfg);
    for parameter in 0..8 {
        sv.delta[parameter] = get_parameters(&cfg)[parameter] * cfg.interval_end;
    }

    if cfg.parameter_variation == "none" {
        let mut out = SamplingOutput {
            me: vec![0; cfg.runs],
            ac: vec![0; cfg.runs],
            activation_times: vec![vec![0.0; 2]; cfg.runs], // for two regions of interest aka genes
            p_value: 1.0,
        };
        let dummy = get_samples(&mut sv, &mut cfg);
    } else if cfg.parameter_variation == "descent" {
        parameter_descent(&mut cfg, &mut sv)
    } else if cfg.parameter_variation2dim == "none" {
        // one dimensional parameter variation
        parameter_variation(&mut cfg, &mut sv);
    } else {
        // two dimensional parameter variation
        parameter_variation(&mut cfg, &mut sv);
    }

    println!(
        "total of {} runs in {} seconds",
        sv.simulation_counter,
        sv.start_time.elapsed().as_secs()
    );
    Ok(())
}

fn get_parameters(cfg: &Parameters) -> Array1<f64> {
    arr1(&[
        cfg.cooperative_add_me_ac[ME],
        cfg.cooperative_add_me_ac[AC],
        cfg.cooperative_del_me_ac[ME],
        cfg.cooperative_del_me_ac[AC],
        cfg.random_addition_me_ac[ME],
        cfg.random_addition_me_ac[AC],
        cfg.random_deletion_me_ac[ME],
        cfg.random_deletion_me_ac[AC],
    ])
}
fn set_parameters(raw_pars: &Array1<f64>, mut cfg: &mut Parameters) {
    // never take negative parameter values ...
    let mut pars = vec![0.0; 8];
    for (i, par) in raw_pars.iter().enumerate() {
        if *par < 0.0 {
            pars[i] = 0.0;
        } else {
            pars[i] = *par
        }
    }
    cfg.cooperative_add_me_ac[ME] = pars[0];
    cfg.cooperative_add_me_ac[AC] = pars[1];
    cfg.cooperative_del_me_ac[ME] = pars[2];
    cfg.cooperative_del_me_ac[AC] = pars[3];
    cfg.random_addition_me_ac[ME] = pars[4];
    cfg.random_addition_me_ac[AC] = pars[5];
    cfg.random_deletion_me_ac[ME] = pars[6];
    cfg.random_deletion_me_ac[AC] = pars[7];

    println!("parameters set to:\n {}", get_parameters(&cfg));
}

fn cfg_str(mut cfg: &mut Parameters) -> String {
    let string = format!("{:?}", cfg);
    string
}

fn descent_header(mut cfg: &mut Parameters) -> String {
    format!(
        "#{}\nname\tscore\tconfidence\tcooperative_me_adder\tcooperative_ac_adder\tcooperative_me_deleter\tcooperative_ac_deleter\trandom_me_adder\trandom_ac_adder\trandom_me_deleter\trandom_ac_deleter",
        cfg_str(&mut cfg),
    )
}
fn descent_entry(mut cfg: &mut Parameters) -> String {
    let string = format!(
        "{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}",
        cfg.cooperative_add_me_ac[ME],
        cfg.cooperative_add_me_ac[AC],
        cfg.cooperative_del_me_ac[ME],
        cfg.cooperative_del_me_ac[AC],
        cfg.random_addition_me_ac[ME],
        cfg.random_addition_me_ac[AC],
        cfg.random_deletion_me_ac[ME],
        cfg.random_deletion_me_ac[AC],
    );
    string
}
/// sample transitions at parameter position
fn get_samples(mut sv: &mut Simvars, mut cfg: &mut Parameters) -> Vec<SamplingOutput> {
    let mut transitions = vec![vec![0.0; 2]; 2];

    set_varied_parameter(
        "origin_of_replication".to_string(),
        cfg.origins_of_replication[0] as f64,
        &mut cfg,
        &mut sv,
    );
    // first descendant with origin
    let mut generations = 0;
    // for descent we wand the lagging strand to change in the first generation but the leading
    // strand should be stable for many generations
    if false && cfg.parameter_variation == "descent" {
        generations = cfg.generations;
        cfg.generations = 1;
    }
    let samples0 = initstring_sampling(&mut sv, &mut cfg, CELL_A);
    if false && cfg.parameter_variation == "descent" {
        cfg.generations = generations;
    }

    // second descendant with origin
    let samples1 = initstring_sampling(&mut sv, &mut cfg, CELL_B);
    // for the  comparison between same strand genes and opposite strand genes
    // turn off the origin

    set_varied_parameter("origin_of_replication".to_string(), 0.0, &mut cfg, &mut sv);

    // first descendant without origin
    let samples2 = initstring_sampling(&mut sv, &mut cfg, CELL_A);
    if cfg.parameter_variation == "none" {
        save_endposition_counts(&mut sv, &mut cfg, CELL_A);
    }
    // second  descendant without origin
    let samples3 = initstring_sampling(&mut sv, &mut cfg, CELL_B);
    let samples = vec![samples0, samples1, samples2, samples3];
    if cfg.parameter_variation == "none" {
        // write out single stability plot data
        save_end_categories_wrt_startstate(&samples, &mut sv, &mut cfg, CELL_A);
        save_activation_times_wrt_startstate(&samples, &mut sv, &mut cfg, CELL_A);
        save_endposition_counts(&mut sv, &mut cfg, CELL_B);
    }
    samples
}

fn get_gradient(mut sv: &mut Simvars, mut cfg: &mut Parameters) -> (Array1<f64>, f64, f64) {
    let mut gradient = arr1(&[0.0; 8]);
    let mut score = 0.0;
    let mut confidence = 0.0;
    for parameter in 0..8 {
        if sv.delta[parameter] < 0.000001 {
            continue;
        }
        // right now the direction vectors are orthogonal, the chosen way is flexible
        let dir_vec = parameter_unit_vector(parameter) * sv.delta[parameter];

        let position = get_parameters(&cfg);
        println!("direction vector: {}", dir_vec);
        let (mut mid_score, mut score_delta_n, mut score_delta_p, mut mid_confidence) =
            get_score_delta(&dir_vec, &mut sv, &mut cfg);

        // is the data significant ??
        confidence = mid_confidence;

        if score_delta_p.abs() > confidence {
            score_delta_p = score_delta_p - confidence * score_delta_p.signum();
        } else {
            score_delta_p = 0.0;
        }
        if score_delta_n.abs() > confidence {
            score_delta_n = score_delta_n - confidence * score_delta_n.signum();
        } else {
            score_delta_n = 0.0;
        }
        if score_delta_n * score_delta_p > 0.0 {
            // if both deltas in same dir go to the better one
            set_parameters(
                &(&position + &(0.9 * &dir_vec * score_delta_p.signum())),
                &mut cfg,
            );
        } else if score_delta_n * score_delta_p == 0.0 {
            // at least one is 0

            if score_delta_p == score_delta_n {
                // both are zero  increase delta and samplesize
                sv.delta[parameter] = (2.0 * sv.delta[parameter]).min(1.0);
                cfg.runs = cfg.runs * 110 / 100;
                println!(
                    "increased delta to {} and samples to {}",
                    sv.delta[parameter], cfg.runs
                );
            } else if score_delta_p == 0.0 {
                set_parameters(
                    &(&position + &(0.5 * &dir_vec * score_delta_n.signum())),
                    &mut cfg,
                );
            } else {
                set_parameters(
                    &(&position + &(0.5 * &dir_vec * score_delta_p.signum())),
                    &mut cfg,
                );
            }
        } else {
            // both are significant but point in different directions, ie we are close to optimum
            let direction = 0.5 * (score_delta_n + score_delta_p).abs()
                / (score_delta_n.abs() + score_delta_p.abs());
            let step = (score_delta_n + score_delta_p).signum();
            set_parameters(
                &(&position + &(&0.9 * step * &dir_vec * direction)),
                &mut cfg,
            );
            println!(
                "{}  at {} is near optimized",
                parameter_name(parameter),
                position[parameter]
            )
        }
        if score_delta_n.abs() + score_delta_p.abs() > 3.0 * confidence {
            let factor =
                confidence / (score_delta_n.abs() + score_delta_p.abs() - 2.0 * confidence);
            sv.delta[parameter] = factor * sv.delta[parameter];
            println!("decreased delta to  {}", sv.delta[parameter]);
        }

        gradient[parameter] = (score_delta_n + score_delta_p) / sv.delta[parameter];
        score = mid_score;
    }
    (gradient, score, confidence)
}

fn get_score_delta(
    dir_vec: &Array1<f64>,
    mut sv: &mut Simvars,
    mut cfg: &mut Parameters,
) -> (f64, f64, f64, f64) {
    let mut current_position = get_parameters(&cfg);
    let mut transitions = vec![vec![vec![0.0; 2]; 2]; 3];
    let alpha = 3.0;
    let tscore95 = 1.960; // 95% two sided confidence for large samples.
    let tscore99 = 2.576; // thats 99% two-sided confidence for large samples, which we have anyway
    const POS: usize = 0;
    const NEG: usize = 1;
    const MID: usize = 2;
    const MEAN: usize = 0;
    const SDEV: usize = 1;

    set_parameters(&current_position, &mut cfg);
    let mut samples = get_samples(&mut sv, &mut cfg);
    transitions[MID][CELL_A][MEAN] = mean(&samples[CELL_A].ac).unwrap();
    transitions[MID][CELL_A][SDEV] = std_deviation(&samples[CELL_A].ac).unwrap();
    transitions[MID][CELL_B][MEAN] = mean(&samples[CELL_B].ac).unwrap();
    transitions[MID][CELL_B][SDEV] = std_deviation(&samples[CELL_B].ac).unwrap();
    let mid_score = transitions[MID][CELL_A][MEAN] - (transitions[MID][CELL_B][MEAN] * alpha);

    set_parameters(&(current_position.clone() + dir_vec), &mut cfg);
    samples = get_samples(&mut sv, &mut cfg);
    transitions[POS][CELL_A][MEAN] = mean(&samples[CELL_A].ac).unwrap();
    transitions[POS][CELL_A][SDEV] = std_deviation(&samples[CELL_A].ac).unwrap();
    transitions[POS][CELL_B][MEAN] = mean(&samples[CELL_B].ac).unwrap();
    transitions[POS][CELL_B][SDEV] = std_deviation(&samples[CELL_B].ac).unwrap();
    let pos_score = transitions[POS][CELL_A][MEAN] - (transitions[POS][CELL_B][MEAN] * alpha);

    set_parameters(&(current_position.clone() - dir_vec), &mut cfg);
    samples = get_samples(&mut sv, &mut cfg);
    transitions[NEG][CELL_A][MEAN] = mean(&samples[CELL_A].ac).unwrap();
    transitions[NEG][CELL_A][SDEV] = std_deviation(&samples[CELL_A].ac).unwrap();
    transitions[NEG][CELL_B][MEAN] = mean(&samples[CELL_B].ac).unwrap();
    transitions[NEG][CELL_B][SDEV] = std_deviation(&samples[CELL_B].ac).unwrap();
    let neg_score = transitions[NEG][CELL_A][MEAN] - (transitions[NEG][CELL_B][MEAN] * alpha);

    // reset parameters to the value of before
    set_parameters(&current_position, &mut cfg);

    let neg_combined_stddev =
        transitions[NEG][CELL_A][SDEV] + alpha * transitions[NEG][CELL_B][SDEV];
    let pos_combined_stddev =
        transitions[POS][CELL_A][SDEV] + alpha * transitions[POS][CELL_B][SDEV];

    // and now to the confidence fun
    let pooled_stddev_combined =
        ((neg_combined_stddev.powf(2.0) + pos_combined_stddev.powf(2.0)) / 2.0).sqrt();

    let confidence_interval_95 = tscore95 * pooled_stddev_combined / (cfg.runs as f64).sqrt();
    let mut delta_p = pos_score - mid_score;
    let mut delta_n = mid_score - neg_score;

    println!("score-   : {}", neg_score);
    println!("score     : {}", mid_score);
    println!("score+    : {}", pos_score);
    println!("0.95 confidence intervall : {}", confidence_interval_95);
    println!(
        "lagging transitions:  {} +-{} \t leading transitions: {} +-{}",
        transitions[MID][CELL_A][MEAN],
        transitions[MID][CELL_A][SDEV],
        transitions[MID][CELL_B][MEAN],
        transitions[MID][CELL_B][SDEV],
    );

    (mid_score, delta_n, delta_p, confidence_interval_95)
}

fn parameter_descent(mut cfg: &mut Parameters, mut sv: &mut Simvars) {
    let mut parameter_descent_outfile = descent_header(&mut cfg);
    let filename = format!("parameter_descent_{}.txt", cfg.name,);
    let mut file = OpenOptions::new()
        .create(true)
        .append(true)
        .open(&filename)
        .unwrap();
    writeln!(file, "{}", parameter_descent_outfile)
        .expect("unable to write variation summary to file");

    let mut transitions = vec![0.0; 2];
    let mut standard_deviation = vec![0.0; 2];

    let (mut new_score, mut current_score, mut new_stddev, mut current_stddev) =
        (0.0, 0.0, 0.0, 0.0);
    for step in 0..100 {
        println!(
            "runtime  {} seconds, iteration {} ",
            sv.start_time.elapsed().as_secs(),
            step
        );
        let (gradient, score, confidence) = get_gradient(&mut sv, &mut cfg);

        writeln!(
            file,
            "\n{}\t{}\t{}\t{}",
            cfg.name.to_string(),
            score,
            confidence,
            descent_entry(&mut cfg)
        )
        .expect("unable to write variation summary to file");
    }
}

fn parameter_variation(mut cfg: &mut Parameters, mut sv: &mut Simvars) {
    let mut parameter_variation_outfile = format!(
        "#{:?}\n{}\t{}\tcell_type\tdescendant\tme_mean\tme_sd\tac_mean\tac_sd\tp_value",
        cfg, cfg.parameter_variation, cfg.parameter_variation2dim
    );

    // its stringsize + 1 as a count includes 0 and stringsize
    let mut category_wrt_startstate = vec![vec![0; cfg.runs]; 2];

    //let center = get_value_of_varied_parameter(&cfg.parameter_variation, &cfg);
    let center = 1.0;
    let base = (cfg.interval_end / center).powf(1.0 / (cfg.intervals as f64 / 2.0).floor() as f64);
    //let center2 = get_value_of_varied_parameter(&cfg.parameter_variation2dim, &cfg);
    let center2 = 1.0;
    let base2 =
        (cfg.interval_end_2dim / center2).powf(1.0 / (cfg.intervals as f64 / 2.0).floor() as f64);

    for c in 0..=cfg.intervals {
        sv.interval_counter = c;
        let new_value = center * base.powf(c as f64 - (cfg.intervals as f64 / 2.0).floor());

        set_varied_parameter(
            cfg.parameter_variation.clone(),
            new_value,
            &mut cfg,
            &mut sv,
        );

        if cfg.parameter_variation2dim != "none" {
            for d in 0..=cfg.intervals {
                sv.interval_counter = c * d;
                let new_value =
                    center2 * base2.powf(d as f64 - (cfg.intervals as f64 / 2.0).floor());

                /*
                if new_value > upper {
                    println!(
                        "reached the end of the parameter intervalal though it would have run further"
                    );
                    break;
                }
                */

                set_varied_parameter(
                    cfg.parameter_variation2dim.clone(),
                    new_value,
                    &mut cfg,
                    &mut sv,
                );
                sv.iterations = 0;

                // ACTUAL SIMULATION:
                let sample = get_samples(&mut sv, &mut cfg);
                parameter_variation_outfile
                    .push_str(&parameter_variation_statistics(&sample, &mut sv, &mut cfg));
                status_message(&mut cfg, &mut sv);

            }
        } else {
            sv.iterations = 0;

            // ACTUAL SIMULATION:
            let sample = get_samples(&mut sv, &mut cfg);
            parameter_variation_outfile
                .push_str(&parameter_variation_statistics(&sample, &mut sv, &mut cfg));
            status_message(&mut cfg, &mut sv);
        }
    }

    let mut filename = format!(
        "{}parameter_variation_{}_{}.txt",
        cfg.folder, cfg.name, cfg.parameter_variation
    );
    fs::write(&filename, parameter_variation_outfile)
        .expect("unable to write variation summary to file");
    println!("output written to: {}", filename);
}

fn status_message(mut cfg: &mut Parameters, mut sv: &mut Simvars) {
    println!(
        "new run with {} = {:.6}  and {} = {:.6}\n runtime so far {} s run: {} ms\n restore steps: {}\n parssum: {}",
        cfg.parameter_variation,
        get_value_of_varied_parameter(&cfg.parameter_variation, &cfg),
        cfg.parameter_variation2dim,
        get_value_of_varied_parameter(&cfg.parameter_variation2dim, &cfg),
        sv.start_time.elapsed().as_secs(),
        sv.single_run_time.elapsed().as_millis(),
        sv.iterations,
        parameter_sum(&cfg),
    );
        sv.single_run_time =  time::Instant::now();
}
fn parameter_sum(cfg: & Parameters) -> f64 {
(cfg.cooperative_add_me_ac[ME]+
cfg.random_addition_me_ac[ME])*(
cfg.cooperative_del_me_ac[ME]+
cfg.random_deletion_me_ac[ME])+(
cfg.cooperative_add_me_ac[AC]+
cfg.random_addition_me_ac[AC])*(
cfg.cooperative_del_me_ac[AC]+
cfg.random_deletion_me_ac[AC])
}

fn set_varied_parameter(
    parameter: String,
    value: f64,
    mut cfg: &mut Parameters,
    mut sv: &mut Simvars,
) {
    match parameter.as_str() {
        "cooperative_me_adder" => cfg.cooperative_add_me_ac[ME] = value,
        "cooperative_ac_adder" => cfg.cooperative_add_me_ac[AC] = value,
        "cooperative_me_deleter" => cfg.cooperative_del_me_ac[ME] = value,
        "cooperative_ac_deleter" => cfg.cooperative_del_me_ac[AC] = value,
        "random_me_adder" => cfg.random_addition_me_ac[ME] = value,
        "random_ac_adder" => cfg.random_addition_me_ac[AC] = value,
        "random_me_deleter" => cfg.random_deletion_me_ac[ME] = value,
        "random_ac_deleter" => cfg.random_deletion_me_ac[AC] = value,
        "time" => cfg.time = value,
        "depletion_bias" => cfg.depletion_bias = value,
        "origin_of_replication" => {
            sv.lead_lag_mask = oris2lead_lag_mask(&vec![value as usize], &[CELL_B, CELL_A], &cfg);
        }
        _ => assert!(false,
            "'{}' is not a paramter to be set",
            cfg.parameter_variation
        ),
    }
}

fn get_value_of_varied_parameter(parameter: &String, cfg: &Parameters) -> f64 {
    let mut ret = 0.0;
    match parameter.as_str() {
        "cooperative_me_adder"   => ret = cfg.cooperative_add_me_ac[ME],
        "cooperative_ac_adder"   => ret = cfg.cooperative_add_me_ac[AC],
        "cooperative_me_deleter" => ret = cfg.cooperative_del_me_ac[ME],
        "cooperative_ac_deleter" => ret = cfg.cooperative_del_me_ac[AC],
        "random_me_adder"   => ret = cfg.random_addition_me_ac[ME],
        "random_ac_adder"   => ret = cfg.random_addition_me_ac[AC], 
        "random_me_deleter" => ret = cfg.random_deletion_me_ac[ME],
        "random_ac_deleter" => ret = cfg.random_deletion_me_ac[AC],
        "time" => ret =  cfg.time as f64,
        "depletion_bias"  => ret = cfg.depletion_bias ,
        "origin_of_replication"  =>  ret =  cfg.origins_of_replication[0] as f64, 
        "none"  => ret = 0.0,
        _ => assert!(false,
            "'{}' is not a paramter to be varied, possible options are :\n cooperative_me_adder\n cooperative_ac_adder \ncooperative_me_deleter \ncooperative_ac_deleter \nrandom_me_adder \nrandom_ac_adder \nrandom_me_deleter \nrandom_ac_deleter \nrandom \ntime \ndepletion_bias \norigin_of_replication",
            parameter
        ),
        }
    ret
}
fn parameter_name(idx: usize) -> String {
    let mut ret = "";
    match idx {
        0 => ret = "cooperative_me_adder",
        1 => ret = "cooperative_ac_adder",
        2 => ret = "cooperative_me_deleter",
        3 => ret = "cooperative_ac_deleter",
        4 => ret = "random_me_adder",
        5 => ret = "random_ac_adder",
        6 => ret = "random_me_deleter",
        7 => ret = "random_ac_deleter",
        _ => assert!(false,
            "'{}' is not a valid index, check the code for options ",
            idx
        ),
    }
    ret.to_string()
}

fn parameter_unit_vector(idx: usize) -> Array1<f64> {
    let mut e = arr1(&[0.0; 8]);
    e[idx] = 1.0;
    println!("unit vector {}", e);
    e
}

// contains 4 cells
struct SamplingOutput {
    me: Vec<usize>,
    ac: Vec<usize>,
    activation_times: Vec<Vec<f64>>,
    //hypothesis: bool,
    p_value: f64,
}

fn initstring_sampling(
    mut sv: &mut Simvars,
    mut cfg: &mut Parameters,
    side: usize,
) -> SamplingOutput {
    let mut out = SamplingOutput {
        me: vec![0; cfg.runs],
        ac: vec![0; cfg.runs],
        activation_times: vec![vec![0.0; cfg.runs]; 2], // for two regions of interest aka genes
        p_value: 1.0,
    };
    sv.end_position_count_vec = vec![vec![vec![0; 2]; cfg.stringsize + 1]; cfg.stringsize + 1];
    let mut mod_count = Position { me: 0, ac: 0 };
    let mut cohorte_average_category_wrt_time = vec![vec![0; 2]; cfg.intervals * cfg.generations];
    let header = format!("#{:?}\ntime\tme\tac", cfg);
    let mut singlesteps = header.clone();
    if cfg.print_steps {
        singlesteps.reserve(cfg.intervals * cfg.generations);
    }
    for run in 0..cfg.runs {
        if cfg.sampling == "single run" {
            singlesteps = header.clone();
            sv.simulation_counter += 1;
            sv.histstring = random_string(
                cfg.start_states[run % cfg.start_states.len()],
                &[ME, AC],
                &cfg,
                sv,
            );
            // generations loop
            for gen in 0..cfg.generations {
                deplete(&mut sv, side, &cfg);
                restore(
                    &mut singlesteps,
                    &mut cohorte_average_category_wrt_time,
                    gen,
                    &mut sv,
                    &cfg,
                );
            }
            mod_count = histstring_position(&sv.histstring);
            sv.end_position_count_vec[mod_count.me][mod_count.ac][side] += 1;

            if mod_count.me >= cfg.stringsize * cfg.gene_activation_threshold / 100 {
                out.me[run] += 1;
            } else if mod_count.ac >= cfg.stringsize * cfg.gene_activation_threshold / 100 {
                //print_histstring(& sv.histstring);
                out.ac[run] += 1;
                //println!("run {} cohorte count now {}",run, cohorte_average_category_wrt_time[cfg.intervals*cfg.generations -1][1] )
            }
            out.activation_times[GENE_A][run] = sv.activation_time[GENE_A];
            out.activation_times[GENE_B][run] = sv.activation_time[GENE_B];
        } else if cfg.sampling == "proliferation" {
            let depth = (cfg.runs as f32).log(2.).floor() as usize;
            // reajust the
            cfg.runs = 2_usize.pow(depth as u32);
            sv.histstring = random_string(cfg.start_states[run], &[ME, AC], &cfg, sv);
            let mut outstring = "".to_string();
            proliferate(depth, &mut sv, &mut outstring, &cfg, &mut out, run);
        } else {
            println!("'{}' is  not a valid sampling option", cfg.sampling);
            process::exit(1);
        }
        if cfg.print_steps {
            let mut filename = format!(
                "{}single_time_course_{}_{}_{}.txt",
                cfg.folder, cfg.name, run, side,
            );
            fs::write(&filename, &singlesteps).expect("unable to write single steps to file");
            println!("output written to: {}", filename);
        }
    }
    let mwu = MannWhitneyU::new(
        out.activation_times[GENE_A].iter().map(|x| x.ceil() as i32),
        out.activation_times[GENE_B].iter().map(|x| x.ceil() as i32),
    );
    out.p_value = mwu.test();
    if cfg.parameter_variation == "none" {
        write_average_time_course(&cohorte_average_category_wrt_time, side, &cfg, &sv)
    }
    out
}
fn write_average_time_course(
    cohorte_average_category_wrt_time: &[Vec<usize>],
    side: usize,
    cfg: &Parameters,
    sv: &Simvars,
) {
    let mut activation = format!("#{:?}\ntime\trepressed\tactive", cfg);
    // leve away first record, its odd ...
    for (i, in_sim_tstep) in cohorte_average_category_wrt_time
        .iter()
        .enumerate()
        .take(cfg.intervals * cfg.generations)
        .skip(1)
    {
        activation.push_str(&format!(
            "\n{}\t{}\t{}",
            cfg.time / cfg.intervals as f64 * i as f64,
            in_sim_tstep[ME] as f64 * 100.0 / cfg.runs as f64,
            in_sim_tstep[AC] as f64 * 100.0 / cfg.runs as f64,
        ))
    }
    let filename = format!(
        "{}average_time_course_{}_{}_{}.txt",
        cfg.folder, cfg.name, sv.interval_counter, side,
    );
    fs::write(&filename, activation).expect("unable to write gene activation time course to file");
    println!("output written to: {}", filename);
}
fn save_endposition_counts(sv: &Simvars, mut cfg: &mut Parameters, side: usize) {
    ///  Saves a density map of where simulation ends to file
    let mut outstring = format!("#{:?}\nme\tac\tstrand\tcount\n", cfg);
    let mut pos = "".to_string();
    // go through state space
    for methylations in 0..=cfg.stringsize {
        for acetylations in 0..=(cfg.stringsize - methylations) {
            for strand in CELL_A..=CELL_B {
                outstring.push_str(&format!(
                    "{}\t{}\t{}\t{}\n",
                    methylations,
                    acetylations,
                    strand, // todo make that leading+lagging
                    sv.end_position_count_vec[methylations][acetylations][strand],
                ));
                if sv.end_position_count_vec[methylations][acetylations][strand] > 0 {
                    pos.push_str(&format!(
                        "Position {{me: {}, ac: {}  }}, ",
                        methylations, acetylations
                    ));
                }
            }
        }
    }
    let filename = format!("{}endposition_counts_{}_{}.txt", cfg.folder, cfg.name, side,);
    fs::write(&filename, outstring).expect("unable to write counts to file");
    println!("output written to: {}", filename);
    let filename = format!(
        "{}endpositions_as_struct_{}_{}.txt",
        cfg.folder, cfg.name, side
    );
    fs::write(&filename, pos).expect("unable to write struct to file");
    println!("output written to: {}", filename);
}

fn generate_all_states(cfg: &Parameters) -> Vec<Position> {
    let mut all_start_states = Vec::new();
    for methylations in 0..=cfg.stringsize {
        for acetylations in 0..=(cfg.stringsize - methylations) {
            all_start_states.push(Position {
                me: methylations,
                ac: acetylations,
            });
        }
    }
    all_start_states
}

fn save_end_categories_wrt_startstate(
    sample: &[SamplingOutput],
    sv: &mut Simvars,
    mut cfg: &mut Parameters,
    side: usize,
) {
    ///
    /// # Arguments
    ///
    /// 'category_wrt_startstate' - counts of final fixpoints for each start state
    let mut outstring = format!("#{:?}\nme\tac\tme_wins\tac_wins\n", cfg);
    for (n, run) in sample[1].me.iter().enumerate() {
        outstring.push_str(&format!(
            "{}\t{}\t{}\t{}\n",
            cfg.start_states[n % cfg.start_states.len()].me,
            cfg.start_states[n % cfg.start_states.len()].ac,
            sample[1].me[n],
            sample[1].ac[n],
        ));
    }
    let filename = format!(
        "{}initstring_trajectories_{}_{}.txt",
        cfg.folder, cfg.name, side,
    );
    fs::write(&filename, outstring).expect("unable to write trajectories to file");
    // its not wrt innitstrings but wrt runs, which does not make sense to separate
    println!(
        "beware, this function is broken:   initstring_trajectories written to: {}",
        filename
    );
}
fn save_activation_times_wrt_startstate(
    sample: &[SamplingOutput],
    sv: &mut Simvars,
    mut cfg: &mut Parameters,
    side: usize,
) {
    ///
    /// # Arguments
    ///
    /// 'activation_times' - times after which gene got activated
    let mut outstring = format!("#{:?}\ncell_type\tdescendant\tgene_A\tgene_B\n", cfg);
    for (nchild, child) in ["A", "B"].iter().enumerate() {
        for (ncell, cell) in ["with origin", "without origin"].iter().enumerate() {
            for n in 0..cfg.runs {
                outstring.push_str(&format!(
                    "{}\t{}\t{}\t{}\n",
                    cell,
                    child,
                    sample[nchild + 2 * ncell].activation_times[GENE_A][n],
                    sample[nchild + 2 * ncell].activation_times[GENE_B][n],
                ));
            }
        }
    }
    let filename = format!("{}activation_times_{}.txt", cfg.folder, cfg.name,);
    fs::write(&filename, outstring).expect("unable to write activation times to file");
    println!("activation times written to: {}", filename);
}

fn parameter_variation_statistics(
    sample: &[SamplingOutput],
    mut sv: &mut Simvars,
    mut cfg: &mut Parameters,
) -> String {
    // extract vector column:
    let sidevec = vec!["strand_A", "strand_B"];
    let mut outstring = format! {""};
    for (nchild, child) in ["A", "B"].iter().enumerate() {
        for (ncell, cell) in ["with origin", "without origin"].iter().enumerate() {
            outstring.push_str(&format!(
                "\n{:.6}\t{:.6}\t{}\t{}",
                get_value_of_varied_parameter(&cfg.parameter_variation, &cfg),
                get_value_of_varied_parameter(&cfg.parameter_variation2dim, &cfg),
                cell,
                child,
            ));
            outstring.push_str(&format!(
                "\t{:.4}\t{:.4}\t{:.4}\t{:.4}\t{:+.2e}",
                mean(sample[nchild + 2 * ncell].me.as_slice()).unwrap(),
                percentile_99(sample[nchild + 2 * ncell].me.as_slice()),
                mean(sample[nchild + 2 * ncell].ac.as_slice()).unwrap(),
                percentile_99(sample[nchild + 2 * ncell].ac.as_slice()),
                sample[nchild + 2 * ncell].p_value,
            ));
        }
    }
    outstring
}

fn histstring_position(histstring: &[usize]) -> Position {
    let mut pos = Position { me: 0, ac: 0 };
    for x in histstring.iter() {
        if *x == ME {
            pos.me += 1
        } else if *x == AC {
            pos.ac += 1
        }
    }
    //println!("xhiststring vec: {} {}",pos.me,pos.ac);
    pos
}

fn histstring_state_for_roi(histstring: &[usize], cfg: &Parameters) -> Vec<bool> {
    let mut state = vec![true, true];
    for gene in 0..2 {
        let mut ac = 0;
        let start = cfg.regions_of_interest[gene][0];
        let stop = cfg.regions_of_interest[gene][1];
        for idx in start..stop {
            if histstring[idx] == AC {
                ac += 1;
            }
        }
        if ac > (stop - start) * cfg.gene_activation_threshold / 100 {
            state[gene] = false;
            //println!("gene {} has {} acetylated positions, thus gene is on",gene, ac);
        }
    }
    state
}
fn print_histstring(histstring: &[usize]) {
    for (i, x) in histstring.iter().enumerate() {
        if *x == UN {
            print!("{}{}", color::Fg(color::White), UN);
        } else if *x == AC {
            print!("{}{}", color::Fg(color::Green), AC);
        } else {
            print!("{}{}", color::Fg(color::Red), ME);
        }
    }
    println!("{}", color::Fg(color::White));
}
fn deplete(mut sv: &mut Simvars, side: usize, cfg: &Parameters) {
    for (i, x) in sv.histstring.iter_mut().enumerate() {
        if sv.lead_lag_mask[i] == side {
            if sv.rng.gen::<f64>() < cfg.depletion_bias {
                *x = UN;
            }
        } else if sv.rng.gen::<f64>() < (1.0 - cfg.depletion_bias) {
            *x = UN;
        }
    }
}
// traverse generation tree, last generation is sampled
fn proliferate(
    depth: usize,
    mut sv: &mut Simvars,
    outstring: &mut String,
    cfg: &Parameters,
    mut out: &mut SamplingOutput,
    mut run: usize,
) {
    // let mut category_wrt_startstate = vec![vec![0; cfg.runs]; 2];
    let mut singlesteps = "".to_string();
    let mut cohorte_average_category_wrt_time = vec![vec![0; 2]; cfg.intervals * cfg.generations];
    let generations = 1;
    restore(
        &mut singlesteps,
        &mut cohorte_average_category_wrt_time,
        generations,
        &mut sv,
        &cfg,
    );
    //print_histstring(sv.histstring);

    if depth > 1 {
        // manual deplete:
        let mut histstring_a = sv.histstring.clone();
        let mut histstring_b = vec![0; cfg.stringsize];
        for (i, x) in histstring_a.iter_mut().enumerate() {
            if sv.lead_lag_mask[i] == CELL_B {
                if sv.rng.gen::<f64>() < cfg.depletion_bias {
                    histstring_b[i] = *x;
                    *x = UN;
                }
            } else if sv.rng.gen::<f64>() < (1.0 - cfg.depletion_bias) {
                histstring_b[i] = *x;
                *x = UN;
            }
        }
        sv.histstring = histstring_a;
        proliferate(depth - 1, &mut sv, outstring, &cfg, &mut out, run);
        sv.histstring = histstring_b;
        proliferate(depth - 1, &mut sv, outstring, &cfg, &mut out, run);
    } else {
        let mod_count = histstring_position(&sv.histstring);
        sv.end_position_count_vec[mod_count.me][mod_count.ac][CELL_A] += 1;

        if mod_count.me >= cfg.stringsize * cfg.gene_activation_threshold / 100 {
            out.me[run] += 1;
        } else if mod_count.ac >= cfg.stringsize * cfg.gene_activation_threshold / 100 {
            out.me[run] += 1;
        }
        run = run - 1
    }
}

fn propensity(
    propensity_target: f64,
    neighbours: &mut Vec<Vec<usize>>,
    sv: &mut Simvars,
    cfg: &Parameters,
) -> f64 {
    let mut propensity_runner: f64 = 0.0;
    //println!("{:?}",sv.histstring);
    //println!("{:?}",neighbours);
    //let ten_millis = time::Duration::from_millis(10);
    //thread::sleep(ten_millis);

    for (i, x) in sv.histstring.iter_mut().enumerate() {
        if *x == UN {
            // cooperative adding
            for modification in ME..=AC {
                propensity_runner += (neighbours[i][modification].pow(2)) as f64
                    * cfg.cooperative_add_me_ac[modification];
                if propensity_runner > propensity_target {
                    // acetylate the position
                    *x = modification;
                    for neighbour in neighbours
                        .iter_mut()
                        .take(sv.boarder_lookup[i][1])
                        .skip(sv.boarder_lookup[i][0])
                    {
                        neighbour[modification] += 1;
                        neighbour[UN] -= 1;
                    }
                    return propensity_runner;
                }
            }
            // random adding
            for modification in ME..=AC {
                propensity_runner += cfg.random_addition_me_ac[modification];
                if propensity_runner > propensity_target {
                    *x = modification;

                    for neighbour in neighbours
                        .iter_mut()
                        .take(sv.boarder_lookup[i][1])
                        .skip(sv.boarder_lookup[i][0])
                    {
                        neighbour[modification] += 1;
                        neighbour[UN] -= 1;
                    }

                    return propensity_runner;
                }
            }
        } else {
            // cooperative deletion
            let modification = if *x == ME { AC } else { ME };
            propensity_runner +=
                (neighbours[i][modification]) as f64 * cfg.cooperative_del_me_ac[*x];
            if propensity_runner > propensity_target {
                // delete position
                for neighbour in neighbours
                    .iter_mut()
                    .take(sv.boarder_lookup[i][1])
                    .skip(sv.boarder_lookup[i][0])
                {
                    neighbour[*x] -= 1;
                    neighbour[UN] += 1;
                }
                *x = UN;
                return propensity_runner;
            }

            // random deletion
            propensity_runner += cfg.random_deletion_me_ac[*x];
            if propensity_runner > propensity_target {
                for neighbour in neighbours
                    .iter_mut()
                    .take(sv.boarder_lookup[i][1])
                    .skip(sv.boarder_lookup[i][0])
                {
                    neighbour[*x] -= 1;
                    neighbour[UN] += 1;
                }
                *x = UN;
                return propensity_runner;
            }
        }
    }
    propensity_runner
}
fn init_neighs(mut sv: &mut Simvars, cfg: &Parameters) -> Vec<Vec<usize>> {
    let mut neighbours = vec![vec![0; 3]; cfg.stringsize];
    for (i, x) in sv.histstring.iter().enumerate() {
        for neighbour in neighbours
            .iter_mut()
            .take(sv.boarder_lookup[i][1])
            .skip(sv.boarder_lookup[i][0])
        {
            neighbour[sv.histstring[i]] += 1;
        }
    }
    neighbours
}

fn restore(
    singlesteps: &mut String,
    mut cohorte_average_category_wrt_time: &mut [Vec<usize>],
    generation: usize,
    mut sv: &mut Simvars,
    cfg: &Parameters,
) {
    // store the second positions an enzyme binds to at the index of the first
    let mut enzyme_positions = vec![cfg.stringsize + 1; cfg.stringsize];
    let mut propensity_sum: f64 = 0.0;
    let mut neighbours = init_neighs(&mut sv, &cfg);
    let mut time = 0.0;
    let mut runner = 0;
    sv.iterations = 0;

    while time < cfg.time * (generation as f64 + 1.0) && sv.iterations < 10000 {
        // call propensity with max target to get total sum
        propensity_sum = propensity(std::f64::MAX, &mut neighbours, &mut sv, cfg);
        let propensity_target = sv.rng.gen::<f64>() * propensity_sum;
        let dummy = propensity(propensity_target, &mut neighbours, &mut sv, cfg);
        // forward time  according to gillespie
        let r1 = sv.rng.gen::<f64>();
        let timedelta = (1.0 / propensity_sum) * (1.0 / r1).ln();
        time += timedelta;
        //println!("{}\t{}\t{}",generation ,time,timedelta);
        //print_histstring(&sv.histstring);

        // update switching times (last timepoint where gene was inactive)

        let states = histstring_state_for_roi(&sv.histstring, &cfg);
        if states[GENE_A] {
            sv.activation_time[GENE_A] = time;
        }
        if states[GENE_B] {
            sv.activation_time[GENE_B] = time;
        }

        //    write out steps

        while time > runner as f64 * cfg.time / cfg.intervals as f64 && runner < cfg.intervals {
            let time_index = runner + generation * cfg.intervals;
            let mod_count = histstring_position(&sv.histstring);
            // fill cetegorization  time series
            if mod_count.me >= cfg.stringsize * cfg.gene_activation_threshold / 100 {
                cohorte_average_category_wrt_time[time_index][ME] += 1;
            } else if mod_count.ac >= cfg.stringsize * cfg.gene_activation_threshold / 100 {
                if time_index == cfg.intervals * cfg.generations - 1 {
                    //print_histstring(& sv.histstring);
                }
                cohorte_average_category_wrt_time[time_index][AC] += 1;
            }
            if cfg.print_steps {
                // add data to single run statistics
                singlesteps.push_str(&format!(
                    "\n{}\t{}\t{}",
                    runner as f64 * cfg.time / cfg.intervals as f64 + generation as f64 * cfg.time,
                    mod_count.me,
                    mod_count.ac
                ));
            }
            runner += 1;
        }
        sv.iterations +=1;
    }
}
fn get_boarder_lookup(cfg: &Parameters) -> Vec<Vec<usize>> {
    let mut boarders = vec![vec![0; 2]; cfg.stringsize];
    let mut window_start = 0;
    let mut window_end = 0;
    // start inclusive, end excluseive
    for (position, boarder) in boarders.iter_mut().enumerate().take(cfg.stringsize) {
        if position < cfg.enzymereach + 1 {
            boarder[0] = 0;
            boarder[1] = position + cfg.enzymereach + 1;
        } else if (position + cfg.enzymereach + 1) > cfg.stringsize {
            boarder[0] = position - cfg.enzymereach;
            boarder[1] = cfg.stringsize;
        } else {
            boarder[0] = position - cfg.enzymereach;
            boarder[1] = position + cfg.enzymereach + 1;
        }
    }
    boarders
}

fn oris2lead_lag_mask(boarders: &[usize], values: &[usize], cfg: &Parameters) -> Vec<usize> {
    let mut pos = 0;
    let mut state = 0;
    let mut mask = vec![0; cfg.stringsize];
    for (i, x) in mask.iter_mut().enumerate() {
        if i < boarders[pos] as usize {
            *x = values[state];
        } else {
            state = (state + 1) % 2;
            *x = values[state];
            pos += 1;
            // reached last entry?
            if pos == boarders.len() {
                // make the modify the rest of the string all the same
                for maskitem in mask.iter_mut().take(cfg.stringsize).skip(i + 1) {
                    *maskitem = values[state];
                }
                break;
            }
        }
    }
    mask
}

fn random_string(
    position: Position,
    values: &[usize],
    cfg: &Parameters,
    sv: &mut Simvars,
) -> Vec<usize> {
    let mut mask = vec![UN; cfg.stringsize];
    for (i, x) in mask.iter_mut().enumerate() {
        if i < position.me {
            *x = values[0];
        } else if i < (position.me + position.ac) {
            *x = values[1];
        } else {
        }
    }
    mask.shuffle(&mut sv.rng);
    mask
}

fn printvec(vect: &[usize]) {
    for v in vect.iter() {
        print!("{}", v);
    }
    println!();
}
fn mean(data: &[usize]) -> Option<f64> {
    let sum = data.iter().sum::<usize>() as f64;
    let count = data.len();

    match count {
        positive if positive > 0 => Some(sum / count as f64),
        _ => None,
    }
}
fn mean_f(data: &[f64]) -> Option<f64> {
    let sum = data.iter().sum::<f64>();
    let count = data.len();

    match count {
        positive if positive > 0 => Some(sum / count as f64),
        _ => None,
    }
}

fn std_deviation(data: &[usize]) -> Option<f64> {
    match (mean(data), data.len()) {
        (Some(data_mean), count) if count > 0 => {
            let variance = data
                .iter()
                .map(|value| {
                    let diff = data_mean - (*value as f64);

                    diff * diff
                })
                .sum::<f64>()
                / count as f64;

            Some(variance.sqrt())
        }
        _ => None,
    }
}
fn std_dev(data: &[f64]) -> Option<f64> {
    match (mean_f(data), data.len()) {
        (Some(data_mean), count) if count > 0 => {
            let variance = data
                .iter()
                .map(|value| {
                    let diff = data_mean - (*value as f64);

                    diff * diff
                })
                .sum::<f64>()
                / count as f64;

            Some(variance.sqrt())
        }
        _ => None,
    }
}
fn percentile_99(data: &[usize]) -> f64 {
    //println!("samples: {}",data.len() );
    3.0 * std_deviation(data).unwrap() as f64 / (data.len() as f64).sqrt()
}

fn l2_norm(x: ArrayView1<f64>) -> f64 {
    x.dot(&x).sqrt()
}
/*
fn  mannwhitneyu( x: &[usize], y:&[usize]  ) f64 {
    let n1 = x.len();
    let n2 = y.len();
    let ranked = rankdata( [x, y].concat() )));
    let rankx = ranked[0:n1]
    u1 = n1*n2 + (n1*(n1+1))/2.0 - np.sum(rankx, axis=0)  # calc U for x
    u2 = n1*n2 - u1  # remainder is U for y
    T = tiecorrect(ranked)
    if T == 0:
        raise ValueError('All numbers are identical in mannwhitneyu')
    sd = np.sqrt(T * n1 * n2 * (n1+n2+1) / 12.0)

    meanrank = n1*n2/2.0 + 0.5
    bigu = max(u1, u2)

    z = (bigu - meanrank) / sd
    p = 2 * distributions.norm.sf(abs(z))

    p
}
#use permutation
fn tiecorrect(arr: &[usize])  -> f64 {
    arr.sort();

    idx = np.nonzero( np.r_[True, arr[1:] != arr[:-1], True] ) [0]
    cnt = np.diff(idx).astype(np.float64)

    size = np.float64(arr.size)
    return 1.0 if size < 2 else 1.0 - (cnt**3 - cnt).sum() / (size**3 - size)
}
fn  rankdata( arr: [usize] ){
    let sorter = permutation::permutation::sort(arr);

    arr = arr[sorter]
    obs = np.r_[ True, arr[1:] != arr[:-1]]
    dense = obs.cumsum()[sorter]


    # cumulative counts of each unique value
    count = np.r_[np.nonzero(obs)[0], len(obs)]

    # average method
    return .5 * (count[dense] + count[dense - 1] + 1)
}
*/
use rustats::distributions::{Cdf as _, StandardNormal};
use rustats::fundamental::average;
use std::cmp::Ordering;

/// The alpha of Mann-Whitney U test.
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub enum Alpha {
    /// 0.01
    P01,

    /// 0.05
    P05,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
enum Group {
    X,
    Y,
}

/// Mann-Whitney U test.
#[derive(Debug)]
pub struct MannWhitneyU {
    xn: usize,
    yn: usize,
    counts: Vec<(usize, usize)>,
}

impl MannWhitneyU {
    /// Makes a new `MannWhitneyU` instance.
    pub fn new<X, Y, T>(xs: X, ys: Y) -> Self
    where
        X: Iterator<Item = T>,
        Y: Iterator<Item = T>,
        T: Ord,
    {
        let mut vs = xs
            .map(|x| (x, Group::X))
            .chain(ys.map(|y| (y, Group::Y)))
            .collect::<Vec<_>>();
        vs.sort();

        let n = vs.len();
        let xn = vs.iter().filter(|t| t.1 == Group::X).count();
        let yn = n - xn;

        let mut counts = Vec::with_capacity(vs.len());
        let mut prev = None;
        for (v, group) in vs {
            if prev.as_ref() != Some(&v) {
                counts.push((0, 0));
            }
            if group == Group::X {
                counts.last_mut().unwrap_or_else(|| unreachable!()).0 += 1;
            } else {
                counts.last_mut().unwrap_or_else(|| unreachable!()).1 += 1;
            }
            prev = Some(v);
        }

        Self { xn, yn, counts }
    }

    /// Tests whether there is a statistically significant difference between `xs` and `ys`.
    pub fn test(&self) -> f64 {
        if self.xn < 21 || self.yn < 21 {
            return 1.0;
        }

        let z = self.z();
        (1.0 - StandardNormal.cdf(&z.abs())) * 2.0
    }

    /// Returns `Ordering::Less` if `xs` is statistically less than `ys`, otherwise `Ordering::Greater`.
    ///
    /// If there is no statistically significant difference, this method returns `Ordering::Equal`.

    fn n(&self) -> usize {
        self.xn + self.yn
    }

    fn xyu(&self) -> (f64, f64) {
        let mut xr = 0.0;
        let mut rank = 1;
        for (x, y) in self.counts.iter().cloned() {
            xr += average((rank..).take(x + y).map(|x| x as f64)) * x as f64;
            rank += x + y;
        }
        let yr = (self.n() * (self.n() + 1) / 2) as f64 - xr;

        let xu = xr - (self.xn * (self.xn + 1) / 2) as f64;
        let yu = yr - (self.yn * (self.yn + 1) / 2) as f64;
        (xu, yu)
    }

    fn u(&self) -> f64 {
        let (xu, yu) = self.xyu();
        xu.min(yu)
    }

    fn mu(&self) -> f64 {
        ((self.xn * self.yn) / 2) as f64
    }

    fn au(&self) -> f64 {
        let t = self
            .counts
            .iter()
            .map(|&(x, y)| x + y)
            .map(|t| t * t * t - t)
            .sum::<usize>() as f64;
        let n = self.n() as f64;
        let n1 = self.xn as f64;
        let n2 = self.yn as f64;
        ((n1 * n2 * ((n + 1.0) - t / (n * (n - 1.0)))) / 12.0).sqrt()
    }

    fn z(&self) -> f64 {
        (self.u() - self.mu()) / self.au()
    }
}
