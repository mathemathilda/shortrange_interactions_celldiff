import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
# colorspace: https://python-colorspace.readthedocs.io/en/stable/installation.html
from colorspace.colorlib import HCL
import pandas
import csv
import sys
import subprocess
import seaborn

if len(sys.argv) < 2 :
    filename= "parameter_variation_opt_run32_tAc5_random_me_adder.txt"
else:
    filename = sys.argv[1]

mydata = pandas.read_csv(filename, sep = "\t",header=1 )
# filter out rows where nothing happens
with open(filename, "r" ) as file:
    parameters = file.readline()

runtime = float(parameters.split(", time: ",1)[1].split(",")[0])

switched = mydata.loc[(mydata["gene_B"] < runtime) & (mydata["gene_A"] <
                                                      runtime) ]
ratio = 100 * switched["gene_A"].size / mydata["gene_A"].size

plt.scatter(x=switched["gene_A"], y = switched["gene_B"], alpha = 0.5)

plt.title("cells where both genes activated (%.1f %% )" % ratio )
plt.ylabel("activation time gene B")
plt.xlabel("activation time gene A")



outfile= filename.split(".")[0] + "_scatter_plot.pdf"
plt.savefig(outfile)
bashCommand = "okular " + outfile
process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
output, error = process.communicate()


#plt.figure();
#data.plot();


