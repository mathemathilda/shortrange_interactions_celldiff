#!/usr/bin/env Rscript

source("plot_util.R")
#library(directlabels)
args = commandArgs(trailingOnly=TRUE)
file = parse_args(args,"singlestep_resol_time_course.txt")
mydata = read.csv(file ,sep="\t",skip = 1)
range = max(mydata$ac)
str_length = max(mydata$me)
len =  nrow(mydata)
mdata = melt (data = mydata, id.vars = "time" )
maxtime = max(mdata$time)
maxtime
generations = 6
interval = maxtime/generations 
interval
library(plyr)
library(gridExtra)
mu <- ddply(mdata, "variable", summarise, grp.mean=mean(value))
	
##  plots 
histo <- ggplot(mdata, aes(x=value,fill=variable, color=variable)) +  
	geom_histogram(binwidth=1,position="identity", alpha=0.5) +
	geom_vline(data=mu, aes(xintercept=grp.mean, color=variable),
             linetype="dashed")+
  	#geom_vline(xintercept=seq(interval,maxtime -interval, interval  ), linetype="dotted") +
	#labs(y = "counts", x = "time in arb.unit")  +
	theme(legend.justification = c(1, 1), legend.position = "none") 
#direct.label(margin, method="last.points")
timecourse <- ggplot(mdata, aes(x=time , y = value)) +  
	geom_line(aes(color = variable), size = 0.5) +
  	#geom_vline(xintercept=seq(interval,maxtime -interval, interval  ), linetype="dotted") +
	labs(y = "counts", x = "time in arb.unit")  +
	theme(legend.justification = c(1, 1), legend.position = c(1, 1)) 
#direct.label(margin, method="last.points")

outfile = mutate_filename(file,"")
pdf(outfile)
grid.arrange(timecourse,histo, ncol = 1)
dev.off()
set_exifdata (file, outfile)

system( paste("evince ",outfile))
