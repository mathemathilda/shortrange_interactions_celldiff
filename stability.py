import matplotlib.pyplot as plt
import numpy as np
import subprocess
import sys
import random
import string
import sympy as sm


N = 60
x, y = sm.symbols(' x, y', negative=False)

U = x**2*(N - x- y)  - 5*N*x
V = y**2*(N - x- y)  - 5*N*y

# compute fixed points
equilibria = sm.solve( (U,V), x, y )
print(equilibria)

print( "Equilibria: ")
for (a,b) in equilibria:
    if a>=0 and b>=0 :
        print("%.2f \t%.2f" % (float(a),float(b)))
        print(" first derivative: %.2f \t %.2f" % (
            U.evalf(subs = {x: a + 0.01, y:b + 0.01 } ),
            V.evalf(subs = {x: a + 0.01, y:b + 0.01} ) ))

A, M = np.mgrid[0:N:60j,0:N:60j]

# enzymes:

a_del_rand = -A
m_del_rand = -M
a_del_coop = -A*M
m_del_coop = -A*M

a_add_rand = (N - M- A)
m_add_rand = (N - M- A)
a_add_coop = M**2*(N - M- A)/N
m_add_coop = A**2*(N - M- A)/N


letters = string.ascii_lowercase
ID = ''.join(random.choice(letters) for i in range(4))

filename = "vectorfield_joint_" + ID + ".pdf"

jn=8
kn=8
fig = plt.figure(figsize=(3*jn,3*kn))
gs = fig.add_gridspec(jn, kn, hspace=0, wspace=0)
axs = gs.subplots(sharex='col', sharey='row')
#fig,axs = plt.subplots(jn,kn)

#fig = plt.figure(frameon=False)
#ax = fig.add_subplot(111)
labels = ['random ac deleter','cooperative ac deleter','random ac adder','cooperative ac adder','random me deleter','cooperative me deleter','random me adder','cooperative me adder']

for i in range(jn*kn):
    j=i%jn
    k=i//kn
    # coefficients

    a = [0,0,0,0]
    m = [0,0,0,0]
    if j < 4 :
        a[j] = 1
    else:
        m[j-4] = 1 
    if k < 4 :
        a[k] = 1
    else:
        m[k-4] = 1
    
    name_x = labels[k]
    name_y = labels[j]

    # ODE s

    dAdt =   a[0]*a_del_rand + a[1]*a_del_coop + a[2]*a_add_rand + a[3] *a_add_coop
    dMdt =   m[0]*m_del_rand + m[1]*m_del_coop + m[2]*m_add_rand + m[3] *m_add_coop

    # Plot the streamlines with an appropriate colormap and arrow style
    color = 2 * np.log(np.hypot(dMdt, dAdt))
    # speed = 2 * np.hypot(dMdt, dAdt)

    # set upper right half to zero:
    URH = np.round(1-(M+A)/120)
    dMdt = dMdt*URH
    dAdt = dAdt*URH


    axs[j,k].streamplot(M, A, dMdt, dAdt, color=color, linewidth=1.0, cmap=plt.cm.binary,
              density=1.5, arrowstyle='->', arrowsize=0.5)
    
    axs[j,k].set_ylabel(name_y , rotation = 70 , labelpad = 20)    
    axs[j,k].set_xlabel(name_x , rotation = 10)
    axs[j,k].set_aspect('equal')

for ax in fig.get_axes():
    ax.label_outer()

print("saved to: "+ filename)
plt.savefig(filename, format='pdf', bbox_inches='tight', pad_inches=0)


git_commit = subprocess.check_output('git log | head -n1', shell = True)[:-1].decode("utf-8")
# [:-1] remoes the newline that python puts there

command = "exiftool -overwrite_original" +\
        " -Title='" + filename + ";a: " + str(a) + " b: " + str(m) + "'" +\
" -Author='matilda mayer' -Subject='histone dynamcs simulation' -Producer='"\
+ git_commit + "' " + filename
subprocess.call(command, shell=True )
command2 = "okular " + filename
subprocess.call(command2, shell=True )
#plt.show()
