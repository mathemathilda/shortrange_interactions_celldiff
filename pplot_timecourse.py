import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
# colorspace: https://python-colorspace.readthedocs.io/en/stable/installation.html
from colorspace.colorlib import HCL
import pandas
import csv
import sys
import subprocess
import code

if len(sys.argv) < 2 :
    filename= "out/average_time_course_poster_run0_nosO_0_0.txt"
else:
    filename = sys.argv[1]
with open (filename) as f:
    header=f.readline()

data = pandas.read_csv(filename, sep = "\t",header=1 )


ordinate = data.iloc[:,0]
variate= ordinate.name
ordinate.name='variate'
#plotdata= pandas.concat([ordinate, lead_me, lagging_me, diff ], axis=1, sort=False)
active = data["active"]

repressed = data["repressed"]
# tetradic colours:
k = 140 # start hue
j = 50  # distance close colours
c = 70  # chroma
l = 60  # lightness
cols = HCL(H = [ k, k+j, k+180, k+180+j, 0 ],
           C = [ c,  c ,  c   ,  c   , 0 ],
           L = [ l,  l ,  l   ,  l   , 30 ])

# methylation
plt.plot(ordinate,active,cols(1)[0])
#plt.fill_between(ordinate,lead_me_sd_n, lead_me_sd_p , color = cols(1)[0] , alpha=0.3)
plt.plot(ordinate,repressed,cols(1)[3])
#plt.fill_between(ordinate,lagging_me_sd_n , lagging_me_sd_p , color = cols(1)[1] , alpha=0.3 )

plt.xlabel(variate)
plt.ylabel("cell fraction")


active_legend = mpatches.Patch(color=cols(1)[0], label='active')
repressed_legend =  mpatches.Patch(color=cols(1)[3], label='repressed')
#lag_ac_legend =mpatches.Patch(color=cols(1)[2],label='lag strand ac')
#lead_ac_legend =mpatches.Patch(color=cols(1)[3],label='lead strand ac')
#diff_legend =mpatches.Patch(color=cols(1)[4],label='difference ac')
plt.legend(handles=[active_legend,repressed_legend], title="cell state")
#plt.legend([lead_me,lead_ac,lagging_me,lagging_ac,diff],
#           ["leading strand me","leading strand ac",
#           "lagging strand me","lagging_strand_ac","difference ac"])

#myplot = plotdata.plot(x = 'variate')
#myplot.set_xlabel(variate)

outfile= filename.split(".")[0] + "_plot.pdf"
plt.savefig(outfile)
## set exif data
git_commit = subprocess.run(['git log | head -n1'],shell=True,stdout=subprocess.PIPE)
#code.interact(local=dict(globals(), **locals()))
exif_command = "exiftool -overwrite_original -Creator='" + str(git_commit.stdout) + ", " +  header + "' -Title='" + filename + "' -Author='matilda mayer' -Subject='histone dynamcs simulation' " + filename

subprocess.run(exif_command,shell=True,stdout=subprocess.PIPE)
## open plot
bashCommand = "okular " + outfile
process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
output, error = process.communicate()


#plt.figure();
#data.plot();


