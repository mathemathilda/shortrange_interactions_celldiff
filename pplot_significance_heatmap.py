import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
# colorspace: https://python-colorspace.readthedocs.io/en/stable/installation.html
#from colorspace.colorlib import HCL
#from colorspace import qualitative_hcl
from matplotlib.colors import LogNorm
from matplotlib import cm
from matplotlib.colors import ListedColormap, LinearSegmentedColormap
from scipy.stats import mannwhitneyu
import pandas
import csv
import sys
import subprocess
import seaborn as sns
import code
import numpy as np

if len(sys.argv) < 2 :
    filename= "parameter_variation_opt_run0_DCDF_random_me_adder.txt"
else:
    filename = sys.argv[1]
# colormap
viridis = cm.get_cmap('viridis', 256)
newcolors = viridis(np.linspace(0, 1, 256))
grey = np.array([150/256, 150/256, 150/256, 1])
newcolors[230:, :] = grey
newcmp = ListedColormap(newcolors)

# get simulation time from file header
with open(filename , "r" ) as file:
    parameters = file.readline()
runtime = float(parameters.split(", time: ",1)[1].split(",")[0])


parameter_variation  =  pandas.read_csv(filename, sep = "\t",header=1 )

with_origin_A = parameter_variation.loc[(parameter_variation.cell_type=="with origin") & (parameter_variation.descendant=="A") ]
pval_heatmap = with_origin_A.drop(columns = ["cell_type","descendant","me_mean","me_sd","ac_mean","ac_sd"])
abscissa  = pval_heatmap.columns[0]
ordinate = pval_heatmap.columns[1]
pval_heatmap = pval_heatmap.rename(columns = {pval_heatmap.columns[0]:"x_axis" , pval_heatmap.columns[1]:"y_axis"} )
#pval_heatmap['p_value'] = np.log10(pval_heatmap['p_value'])

# remove 0 values that are most likely doe to rounding
near_min = pval_heatmap.p_value[pval_heatmap.p_value.gt(0)].min(0)
pval_heatmap.p_value[pval_heatmap.p_value == 0] = near_min

# pandas pivot with multiple variables
heatmap_data = pandas.pivot_table(pval_heatmap, values='p_value', index=['y_axis'], columns='x_axis')# leading same side

#code.interact(local=dict(globals(), **locals()))
fig, ax = plt.subplots(figsize=(9, 6))
print(pval_heatmap.p_value.max())
print(pval_heatmap.p_value.min())
#sns.heatmap(heatmap_data,  norm=LogNorm(vmin=pval_heatmap.p_value.min(),vmax=pval_heatmap.p_value.max()) ,cmap="hot")
sns.heatmap(heatmap_data,norm=LogNorm(vmin=1e-14,vmax=1) ,cmap=newcmp,square=True)
#sns.set_style('white')
plt.xlabel(abscissa, size=14)
plt.ylabel(ordinate, size=14)
#ax.set_xticklabels(['{:e}'.format(float(t.get_text())) for t in
#                    ax.get_xticklabels()])
#ax.set_yticklabels(['{:.3f}'.format(float(t.get_text())) for t in ax.get_yticklabels()])
ticks = [float(t.get_text()) for t in ax.get_xticklabels()]
m = int(np.log10(max(ticks)))
print(m)
sparseticks = [10**t for t in range(-m,m+1)]
#print(sparseticks)
ticks= ['' for t in ticks]
ticks[::int(len(ticks)/(len(sparseticks)-1))] = sparseticks
ax.set_yticklabels(ticks)
ax.set_xticklabels(ticks)
ax.invert_yaxis()
plt.title("measurable differences", size=14)
plt.tight_layout()

outfile= "images/" + filename.split(".")[0] + "_significance_plot.pdf"
plt.savefig(outfile,dpi=150, figsize=(9,6))


bashCommand = "okular " + outfile
process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
output, error = process.communicate()

#code.interact(local=dict(globals(), **locals()))




#sns.set_style('whitegrid')
#fig, ax = plt.subplots(figsize=(9, 6))
#sns.violinplot(x= "time", y="run", hue="gene", data=splitdata, palette = "hls", split=True)


#_ = plt.xticks(rotation=45, ha='right')
#plt.title("cells where both genes activated ({} same strand) ({:.1f} opposite strand) ".format(  ratio_same,  ratio_opos) )
#plt.ylabel("activation time in a.u.")
#plt.subplots_adjust(bottom=0.2)






#plt.figure();
#data.plot();


