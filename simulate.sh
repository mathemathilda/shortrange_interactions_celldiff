#!/bin/bash  -x

# get run number
RUN_NUMBER=$(( $(tail runlog.txt -n 1) + 1 ))

./git_update.sh

git log -1 --oneline >> runlog.txt
echo $RUN_NUMBER >> runlog.txt
git add runlog.txt

#cargo run --release -- $RUN_NUMBER 2>&1 | tee -a log/$RUN_NUMBER.log &
cargo run --release -- $RUN_NUMBER 2>&1 >  $RUN_NUMBER.log &
