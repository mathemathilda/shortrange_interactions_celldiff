#!/usr/bin/env Rscript
args = commandArgs(trailingOnly=TRUE)
require(ggplot2)
require(ggExtra)
require(stringr)
require(ggpubr)
source("plot_util.R")
# test if there is at
if (length(args)==0) {
  # default input  file
	file = "endposition_counts_1_0.txt"
} else if (length(args)==1) {
	file = args[1]
}
mdata = read.csv(file ,sep="\t",skip = 1)
range = max(mdata$me)


strand0 = subset(mdata, strand ==0)[c(1,2,4)]
strand1 = subset(mdata, strand ==1)[c(1,2,4)]

mydata <- merge(strand0,strand1,by=c("me","ac"))



	
##  plots 
outfile = mutate_filename(file,"2D")
pdf(outfile)
density <- ggplot(mydata) + 
	geom_raster( aes(x=me,y=ac,fill=count.x + count.y)) +
	scale_fill_distiller(name = "counts",palette=4, trans = "log", direction=1) +
       	# geom_point( aes(me,ac,color=ac_wins)) +
       	# scale_color_gradient2(midpoint=5, low="blue", mid="white", high="red", space ="Lab" ) + 
	theme(legend.justification = c(1, 1), legend.position = c(1, 1)) +
       	coord_fixed(ratio = 1) 
plot(density)
dev.off()

set_exifdata (file, outfile)
system( paste("okular ",outfile))
