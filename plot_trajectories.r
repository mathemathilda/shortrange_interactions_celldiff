#!/usr/bin/env Rscript

source("plot_util.R")

args = commandArgs(trailingOnly=TRUE)
file = parse_args(args,"")
args = commandArgs(trailingOnly=TRUE)
mydata = read.csv(file ,sep="\t",skip = 1)
runs = max(mydata$ac_wins)
range = max(mydata$ac)
acetylated = mydata[mydata[,"ac_wins"] >=runs*6/10,] 
methylated = mydata[mydata[,"me_wins"] >=runs*6/10,] 
super_acetylated = mydata[mydata[,"ac_wins"] >=runs*95/100,] 
super_methylated = mydata[mydata[,"me_wins"] >=runs*95/100,] 
bistable = mydata[mydata[,"me_wins"] >0,] 
bistable = bistable[bistable[,"ac_wins"] >0,]
##  plots 


#### super stable regions, ie where (5 of the runns end up in the same state
superstable <-  ggplot(acetylated) +
	geom_point(aes(me,ac ),color = "green4", size = 2.5) + 
	geom_point(data = methylated , aes(me, ac) ,colour= "red4",size = 2.5) +
	geom_point(data = super_acetylated , aes(me, ac) ,colour= "green",size = 2.5) +
	geom_point(data = super_methylated , aes(me, ac) ,colour= "tomato",size = 2.5) +
	geom_point(data = bistable, aes(me,ac) ,colour = "black",size = 1.5) +
	coord_fixed(ratio = 1) 

plot(superstable)
outfile = mutate_filename(file,"bistability_plot")
ggsave(outfile )


#### heatmap style
detailed <- ggplot(mydata) +
       	geom_raster( aes(me,ac,fill=me_wins + ac_wins)) +
       	scale_fill_gradient2(midpoint=runs/2, low="blue", mid="white", high="red", space ="Lab" ) +
       	# geom_point( aes(me,ac,color=ac_wins)) +
       	# scale_color_gradient2(midpoint=5, low="blue", mid="white", high="red", space ="Lab" ) + 
       	coord_fixed(ratio = 1) 

plot(detailed)
ggsave("bistability_detailed.pdf", plot = detailed, width = 10)

set_exifdata (file, outfile)
system( paste("evince ",outfile))



