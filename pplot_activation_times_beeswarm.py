import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
# colorspace: https://python-colorspace.readthedocs.io/en/stable/installation.html
from colorspace.colorlib import HCL
from colorspace import qualitative_hcl
from scipy.stats import mannwhitneyu
import pandas
import csv
import sys
import subprocess
import seaborn as sns
import code

if len(sys.argv) < 2 :
    filename= "data/activation_times_opt_run36_mKBS"
else:
    filename = sys.argv[1]

# get simulation time from file header
with open(filename , "r" ) as file:
    parameters = file.readline()
runtime = float(parameters.split(", time: ",1)[1].split(",")[0])


all_cells =  pandas.read_csv(filename, sep = "\t",header=1 )


# leading same side
sameside1 = all_cells.loc[(all_cells.cell_type=="without origin") & (all_cells.descendant=="A")]
# lagging same side
sameside2 = all_cells.loc[(all_cells.cell_type=="without origin") & (all_cells.descendant=="B")]
# leading opposite
opposite1 = all_cells.loc[(all_cells.cell_type=="with origin") & (all_cells.descendant=="A")]
# lagging opposite
opposite2 = all_cells.loc[(all_cells.cell_type=="with origin") & (all_cells.descendant=="B")]

# we  cant distinguish them later, so combine ...
sameside = sameside1.append(sameside2)
opposite = opposite1.append(opposite2)
# filter out rows where nothing happens
# only cells where both genes turned on ...

switched = all_cells.loc[(all_cells["gene_B"] < runtime) & (all_cells[ "gene_A" ] < runtime) ]
switched_same = sameside.loc[(sameside["gene_B"] < runtime) & (sameside[ "gene_A" ] < runtime) ]
switched_opos = opposite.loc[(opposite["gene_B"] < runtime) & (opposite[ "gene_A" ] < runtime) ]
switched_same1 = sameside1.loc[(sameside1["gene_B"] < runtime) & (sameside1[ "gene_A" ] < runtime) ].drop(columns=["cell_type","descendant"])
switched_opos1 = opposite1.loc[(opposite1["gene_B"] < runtime) & (opposite1[ "gene_A" ] < runtime) ].drop(columns=["cell_type","descendant"])
switched_same2 = sameside2.loc[(sameside2["gene_B"] < runtime) & (sameside2[ "gene_A" ] < runtime) ].drop(columns=["cell_type","descendant"])
switched_opos2 = opposite2.loc[(opposite2["gene_B"] < runtime) & (opposite2[ "gene_A" ] < runtime) ].drop(columns=["cell_type","descendant"])

ratio_same = 100 * switched_same["gene_A"].size / sameside["gene_A"].size
ratio_opos = 100 * switched_opos["gene_A"].size / opposite["gene_A"].size
#  absolute difference between activation times:

diffs_same= (sameside["gene_B"] - sameside["gene_A"]).abs()
diffs_opos= (opposite["gene_B"] - opposite["gene_A"]).abs()

# to be able to concatenate we need to reordernumber the index, or it wil
# strugle wit the gaps that come from cells that didn switch at all
switched.reset_index(drop=True, inplace=True)
switched_same.reset_index(drop=True, inplace=True)
switched_opos.reset_index(drop=True, inplace=True)
switched_same1.reset_index(drop=True, inplace=True)
switched_opos1.reset_index(drop=True, inplace=True)
switched_same2.reset_index(drop=True, inplace=True)
switched_opos2.reset_index(drop=True, inplace=True)
diffs_same.reset_index(drop=True, inplace=True)
diffs_opos.reset_index(drop=True, inplace=True)
diffs_same.name = "time delta same strand"
diffs_opos.name = "time delta opposite strand"





switched_opos = switched_opos.rename(columns={"gene_B":"gene_B_opposing","gene_A":"gene_A_opposing"})

splitdata =\
    pandas.concat([ pandas.melt(switched_same1, var_name="gene",
                                value_name="time").assign(run="no ORI 1"),\
                    pandas.melt(switched_same2,var_name="gene",
                                value_name="time").assign(run="no ORI 2"),\
                    pandas.melt(switched_opos1, var_name="gene",
                                value_name="time").assign(run="ORI between 1"),\
                    pandas.melt(switched_opos2, var_name="gene",
                                value_name="time").assign(run="ORI between 2")],
                  axis = 0)
code.interact(local=dict(globals(), **locals()))
# p values
#for the right positioning:
medians = splitdata.groupby(['run'])['time'].median().values
dfs = [rows for _, rows in splitdata.groupby('run')]
result = [[rows for _, rows in df.groupby('gene')] for df in dfs]
pvalues = [ mannwhitneyu(res[0]["time"], res[1]["time"], use_continuity=True, alternative=None).pvalue  for res in result]

splitdata =\
    pandas.concat([ pandas.melt(switched_same1, var_name="gene",
                                value_name="time").assign(run="no ORI 1\n p {:.2e}".format(pvalues[2])),\
                    pandas.melt(switched_same2, var_name="gene",
                                value_name="time").assign(run="no ORI 2\n p = {:.2e}".format(pvalues[3])),\
                    pandas.melt(switched_opos1, var_name="gene", value_name="time").assign(run="ORI between 1\n p = {:.2e}".format(pvalues[0])),\
                    pandas.melt(switched_opos2, var_name="gene", value_name="time").assign(run="ORI between 2\n p = {:.2e}".format(pvalues[1]))],
                  axis = 0)


#code.interact(local=dict(globals(), **locals()))
sns.set_style('whitegrid')
fig, ax = plt.subplots(figsize=(9, 6))
sns.violinplot(x= "time", y="run", hue="gene", data=splitdata, palette = "hls", split=True)
#sns.violinplot(x="time", y="run",data=jointdata, palette = "hls")


#seaborn.boxplot(data=jointdata,showcaps=False, boxprops={'facecolor':'None'},showfliers=False,whiskerprops={'linewidth':0})
#plt.plot(ordinate,diff,cols(1)[4])
_ = plt.xticks(rotation=45, ha='right')
plt.title("cells where both genes activated ({} same strand) ({:.1f} opposite strand) ".format(  ratio_same,  ratio_opos) )
plt.ylabel("activation time in a.u.")
#plt.ylabel("activation time gene B")
#plt.xlabel("activation time gene A")
plt.subplots_adjust(bottom=0.2)




outfile= filename.split(".")[0] + "_beeswarm_plot.pdf"
plt.savefig(outfile)
bashCommand = "okular " + outfile
process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
output, error = process.communicate()


#plt.figure();
#data.plot();


