#!/bin/bash  -x


# determine whether to commit bacause generating files have changed:
COMMIT=false
MODIFIED=$(git status | grep modified | sed 's/modified://')
while read FILENAME ; do
	if [[ $FILENAME == *".r"*  ]] || [[ $FILENAME == *".rst"*  ]] || [[ $FILENAME == *".sh"*  ]] ; then  
		echo "${FILENAME} has changed forcing commit"
		COMMIT="true"
	fi
	git add $FILENAME
done <<< "$MODIFIED"


if $COMMIT  ; then 
	echo " important files changed, new commit";
	echo " please supply commit name:";
	read;
	git commit -m "$REPLY"
fi
