#!/usr/bin/env Rscript
args = commandArgs(trailingOnly=TRUE)
require(ggplot2)
require(stringr)
require(reshape)
require(digest)
# test if there is at
if (length(args)==0) {
	  stop("At least one argument must be supplied (input file).n", call.=FALSE)
} else if (length(args)==1) {
	file = args[1]
}
mdata = read.csv(file ,sep="\t",skip = 1)
params = readLines(file, n=1)
labels = colnames(mdata) 

variate = labels[1]
labels[1] = "parameter"
names(mdata)=labels
git_commit = system("git log | head -n1", intern = TRUE)
range = max(mdata$Counts)
#minim = min(mdata$ some var)

##  plots 

#### super stable regions, ie where (5 of the runs end up in the same state
variation <-  ggplot(mdata, aes(x=parameter, y=pmax(1,Counts), col=Endstate) ) +
	geom_line(aes(linetype=Strand)) +
	labs(x = variate, y = "counts of final state categorisation") +
	#annotate(geom = "text", x=minim  , y = range, label =paste (git_commit, params, sep = "\n") , hjust = 0, vjust = 1, size = 2.5) +
	scale_y_log10(limits = c(1,100)) +
	#scale_y_continuous(trans = 'log10') +
	#ylim(1,2000) +
	annotation_logticks()

plot(variation)
hash = substring(sha1(runif(1,0,1)),1,5)
outfile =  paste(strsplit(file, '[.]')[[1]][1],"_small_parameter_variation_plot_",hash,".pdf",sep="" )
ggsave(outfile )
system(paste("exiftool -overwrite_original -Title='bistability plot of possible histone modification ratios' -Author='matilda mayer' -Subject='histone dynamcs simulation' -Comment='",git_commit," ",params,"' ",outfile))




browseURL(outfile) 
