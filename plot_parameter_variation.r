#!/usr/bin/env Rscript

source("plot_util.R")

args = commandArgs(trailingOnly=TRUE)
file = parse_args(args,"")
mydata = read.csv(file ,sep="\t",skip = 1)
labels = colnames(mydata) 
# make first label uniform and extract what has been varied
variate <- labels[1]
labels[1] = "parameter"
labels[5] = "sd"
colnames(mydata) = labels

range = max(mydata$me, mydata$ac)
minim = min(mydata$parameter)

# construct new array with strand A and diff between strands, only me data is needed, ac is mostly redundant



##  plots 

#### super stable regions, ie where (5 of the runs end up in the same state
variation <-  ggplot(mydata, aes(x=parameter, y=mean, col=state) ) +
	geom_line(aes(linetype=strand)) +
	labs(x = variate, y = "counts of final state categorisation") +
	geom_ribbon( aes(ymin =pmax(mean  - sd,0), ymax = mean + sd, fill = state, linetype =strand), alpha = 0.3) +
	theme(legend.justification = c(1, 1), legend.position = c(1, 1)) 
	#theme(legend.position = "none") 
	#scale_y_log10(limits = c(1,range)) +
	#scale_y_continuous(trans = 'log10') +
	#ylim(1,2000) +
	#annotation_logticks()
plot(variation)
outfile = mutate_filename(file,"")
ggsave(outfile )
set_exifdata (file, outfile)
system( paste("evince ",outfile))

